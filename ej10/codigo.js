function esPalindromo(cadena) {
   var cadenaInvertida = "";
   var cadenaSinEspacios = cadena.split(" ").join("").toLowerCase();

   for (let i = cadenaSinEspacios.length; i >= 0; i--) {
      cadenaInvertida += cadenaSinEspacios.charAt(i);
   }

   return (cadenaInvertida == cadenaSinEspacios) ? true : false; 
}

if (esPalindromo("La ruta nos aporto otro paso natural")) {
   console.log("Es palíndromo");
} else {
   console.log("No es palíndromo");
}