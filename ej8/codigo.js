function esImparPar(n) {
   return (n % 2 == 0) ? "Es par" : "Es impar";
}

console.log(esImparPar(7));
console.log(esImparPar(4));